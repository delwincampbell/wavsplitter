# Splits WAV files into 1 second segments, discards silence
import sys
from pydub import AudioSegment
from pydub.silence import split_on_silence
from pydub.utils import make_chunks

# CONFIG
# length of slice to generate
SLICE_LENGTH = 2000
# length of silence in ms
MIN_SILENCE_LENGTH = 500
# gate threshold in dBFS
THRESHOLD = -24
# ms of silence to keep
KEEP_SILENCE = 250

if (len(sys.argv) < 2):
  sys.exit("No file specified.")

audio = AudioSegment.from_file(str(sys.argv[1]))

chunk = split_on_silence(audio, MIN_SILENCE_LENGTH, THRESHOLD, KEEP_SILENCE)

for i, chunk in enumerate(chunk):
  tiny_chunks = make_chunks(chunk, SLICE_LENGTH)
  for j, tiny_chunk in enumerate(tiny_chunks):
    filename = "{0}-slice-{1}-{2}.mp3".format(sys.argv[1], i, j)
    print("Exporting {0}".format(filename))
    tiny_chunk.export(filename, bitrate = "128k", format = "mp3")

  