# wavsplitter

Trims silence and splits audio files into small segments

Requires ffmpeg & pydub

## Usage

```
python wavsplitter.py example.wav
```

